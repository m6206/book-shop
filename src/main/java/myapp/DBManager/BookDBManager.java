package myapp.DBManager;

import myapp.DB.ConnectionProvider;
import myapp.models.Book;
import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class BookDBManager {

    private int authorId = -1;
    private int bookId = -1;
    Connection connection = ConnectionProvider.getConnection();
    PreparedStatement prepStatement;
    ResultSet resultSet;

    public Book getBook(int bookId) throws SQLException {
        Book theBook = new Book();
        String str = "SELECT * FROM books WHERE id = bookId";
        try{ 
            prepStatement = connection.prepareStatement(str);
            resultSet = prepStatement.executeQuery();
            theBook.setBookName( resultSet.getString( "book_name" ) );
            theBook.setGenre( resultSet.getString( "jenre" ) );
            theBook.setPublishmentYear( resultSet.getString( "publishment_year" ) );
        
            str = "SELECT * FROM author JOIN book ON book.author_id = author.id";
            prepStatement = connection.prepareStatement( str );
            resultSet = prepStatement.executeQuery();
            theBook.setAuthor( resultSet.getString( "name" ) );
        
        }catch( Exception e){
            
        }
        return theBook;
    }
    
    public Book addBook(Book book){
        if( !checkIfAuthorExists( book.getAuthor() ) ) {
            insertAuthorData( book.getAuthor() );
        }
        if( !checkIfBookExists( book ) ){
            insertBookData(book);
        }
        return book;
    }
    
    
    public boolean checkIfBookExists(Book book){
        try{
            String checkIfTheBookAlreadyExists = "SELECT * FROM book WHERE book_name = ? AND jenre = ?";
            prepStatement = connection.prepareStatement( checkIfTheBookAlreadyExists );
            prepStatement.setString( 1, book.getBookName() );
            prepStatement.setString( 2, book.getGenre() );
            resultSet = prepStatement.executeQuery();
            
            if( resultSet.next()){
                return true;
            }
            prepStatement.close();
            
//            connection.close();
        }
        catch( Exception e ){
            e.printStackTrace();
        }
        return false;
    }


    public boolean checkIfAuthorExists(String authorName){
        try{
            prepStatement = connection.
                    prepareStatement( "SELECT id FROM author WHERE name = ?" );
            prepStatement.setString( 1, authorName );
            resultSet = prepStatement.executeQuery();

            if( resultSet.next() ){
                authorId = resultSet.getInt( "id" );
                return true;
            }
        }
        catch(Exception e ){
            e.printStackTrace();
        }
        return false;
    }

   public int insertAuthorData(String authorName){
       String insertAuthorData = "INSERT INTO author(name) VALUES (?);";
       
       
       try { 
           prepStatement = connection.prepareStatement(insertAuthorData);
           prepStatement.setString( 1, authorName );
           resultSet = prepStatement.executeQuery();

           authorId = prepStatement.getGeneratedKeys().getInt( "id" );
           System.out.println(authorId); 
            }
            catch( SQLException e ) {
           e.printStackTrace();
          }
       return authorId;
   } 


public boolean insertBookData( Book book) {
  
    try {
        String bookName = book.getBookName();
        String publishmetYear = book.getPublishmentYear();
        String bookJenre  = book.getGenre();
        int tempId = 7;
        authorId =4;     
        String insertBookData = "INSERT INTO book (book_name, publishment_year, jenre, author_id) VALUES (?, ?, ?, ?)";
        
        
        prepStatement = connection.prepareStatement( insertBookData );
        prepStatement.setString( 1, bookName );
        prepStatement.setString( 2, publishmetYear );
        prepStatement.setString( 3, bookJenre );
        prepStatement.setInt(4, authorId);
        prepStatement.execute();
        connection.commit();
        prepStatement.close();
        
//        connection.close();
//      boolean isExecuted = statement.execute( "INSERT INTO book VALUES(6, \"Namus\", \"1887\", \"drama\", 4);" );
    }
    catch( Exception e ) {
        e.printStackTrace();
    }
    return false;
    }
}

