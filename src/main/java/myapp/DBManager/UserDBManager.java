package myapp.DBManager;

import myapp.DB.ConnectionProvider;
import myapp.models.User;
import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

@Component
public class UserDBManager {

    Connection connection = ConnectionProvider.getConnection();
    PreparedStatement prepStatement;
    ResultSet resultSet;
    
    public boolean updateDBwithBookData( User user ){
        boolean isChecked = false;
        try {
            prepStatement = connection.prepareStatement( "SELECT first_name FROM user WHERE first_name = ? and  last_name = ?" );
            prepStatement.setString( 1, user.getFirstName() );
            prepStatement.setString( 2, user.getLastName() );
            resultSet = prepStatement.executeQuery();
            
                if(!resultSet.next() ) {
                 String insertUserData = "INSERT INTO user(first_name, last_name) VALUES (?, ?);";
                 prepStatement = connection.prepareStatement( insertUserData );
                 prepStatement.setString( 1, user.getFirstName() );
                 prepStatement.setString( 2, user.getLastName() );
                 prepStatement.execute();
                 connection.commit();
                 isChecked = true;
                }
                else{
                    System.out.println( "This user is already registered!" );
                }
                
        }
        catch(Throwable e){
            System.out.println( e.getMessage() );
        }
        return isChecked;
    }
    
}
