package myapp.services;

import myapp.DBManager.UserDBManager;
import myapp.models.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {

    @Autowired
    UserDBManager userDBManager;
    
    public boolean callDBManager( User theUser ){
       return userDBManager.updateDBwithBookData(theUser);
    }
    
    public List<User> getUsers(){
        return null;
    }

    public User getUser(int userId){
        return null;
    }
    
    public User addUser(User user){
        return null;
    }
    
    public User updateUser(User user){
        return null;
    }
    
    public String deleteUser(int userId){
        return null;
    }
}
