package myapp.services;

import myapp.DBManager.BookDBManager;
import myapp.models.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

@Service
public class BookService {
   
    @Autowired
    BookDBManager bookDBManager;
    

    public List<Book> getBooks() {
        //todo temp solution need to get from DB
        Book book = new Book();
        book.setAuthor("asa");
        book.setBookName("name");
        return Arrays.asList(book);
    }

 
    public Book saveBook( Book book ) {
        return bookDBManager.addBook( book);
    }

    
    public Book getBook( int bookId ) {
        return null;
    }

    
    public void update( Book book, String[] params ) {

    }
    
    public String deleteBook( int bookId ) {
        return null;
    }

    private Boolean isUploaded = false;

    /**
     * Validator method 
     */

    public boolean checkTheBook(Book book){
        return bookDBManager.checkIfBookExists(book);

    }

    public void updateDB(Book book){
   
    }

    public boolean uploadBook( MultipartFile file, Book book) {
        String msg= "";
        if(!file.isEmpty()){
            try {
                //TODO GET FILE FROM CONFIG_property DB table
                String fileName = "C:\\Users\\Suren\\Desktop\\library\\" + file.getOriginalFilename();
                // for storing uploaded file
                file.transferTo(new File(fileName));
                msg = "Upload successful for " + file.getName();
                System.out.println(msg);
                isUploaded = true;
            } catch (IllegalStateException e) {
                e.printStackTrace();
            } catch ( IOException e) {
                e.printStackTrace();
            }
        }else {
            msg = "Upload failed for " + file.getName() + " as file is empty";
        }
        if( isUploaded  ){
           bookDBManager.addBook(book);
            return true;
        }

        return false;
    }
}
