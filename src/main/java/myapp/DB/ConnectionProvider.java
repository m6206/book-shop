package myapp.DB;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionProvider {
   
    private final static String dbUrl = "jdbc:mysql://localhost:3306/library_db";
    private final static String user = "student";
    private final static String pass = "password";
    
    private static Connection connection;
    
    public ConnectionProvider() {}
    
    public static Connection getConnection(){
        try {
            Class.forName( "com.mysql.cj.jdbc.Driver" );
            connection = DriverManager.getConnection( dbUrl, user, pass);
            connection.setAutoCommit(false);
        }
        catch( SQLException e ) {
            e.printStackTrace();
        }
        catch( ClassNotFoundException e ) {
            e.printStackTrace();
        }
        return connection;
   }
}
