package myapp.controllers;

import myapp.models.User;
import myapp.services.UserService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;


@RestController
public class UserRestController {
    @Autowired
    private UserService userService;
    
    @GetMapping("/users")
    public List<User> readUsers(){
        return userService.getUsers();
    }
    
    @GetMapping("/users/{userId}")
    public User readUser( @PathVariable int userId ){
        return userService.getUser(userId);
    }
    
    @PostMapping("/users")
    public User createUser(@RequestBody User user){
        userService.addUser(user);
        return user;
    }
    
    @PutMapping("/users")
    public User updateUser(@RequestBody User user){
        userService.updateUser(user);
        return user;
    }
    
    @DeleteMapping("/users/{userId}")
    public String deleteUser(@PathVariable int userId){
        return userService.deleteUser(userId);
    }
    
    @RequestMapping( "/showUserForm" )
    public String showUserForm( Model model ) {
        model.addAttribute( "user", new User() );
        return "user-form";
    }
    
    
    
    @RequestMapping( "/processUserForm" )
    public String processUserForm( @Valid @ModelAttribute("user") User theUser, BindingResult theBindingResult ) {

        if( theBindingResult.hasErrors() ) {
            return "user-form";
        }

        if( StringUtils.equals(theUser.getRole(), "Teacher")){

            return "teacher-actions";
        }

        else {
            return "student-actions";
        }

    }
    
}
