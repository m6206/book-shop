//package myapp.controllers;
//
//import myapp.models.Book;
//import org.springframework.stereotype.Controller;
//import org.springframework.ui.Model;
//import org.springframework.web.bind.annotation.ModelAttribute;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.multipart.MultipartFile;
//
//import java.io.File;
//import java.io.IOException;
//
//@Controller
//public class LibraryController {
//    private String firstName;
//    private String lastName;
//    private String bookName;
//    private String authorName;
//    private String jenre;
//    private String publishmentYear;
//    private int bookId;
//    private int userId;
//    private int authorId;
//
//    @RequestMapping( "/" )
//    public String showIndexPage() {
//        return "index";
//    }
//
//    @RequestMapping("/showBookForm")
//    public String showBookForm( Model model){
//        model.addAttribute("book", new Book());
//        return "book-form";
//    }
//    
//    @RequestMapping(value = "/processForm", method = RequestMethod.POST)
//    public void processForm( @RequestParam("file") MultipartFile file, @ModelAttribute Book book) {
//        System.out.println("inside upload method");
//        String msg= "";
//        if(!file.isEmpty()){
//            try {
//                String fileName = "C:\\Users\\Suren\\Desktop\\library\\" + file.getOriginalFilename();
//                // for storing uploaded file
//                file.transferTo(new File(fileName));
//                msg = "Upload successful for " + file.getName();
//            } catch( IllegalStateException e) {
//                e.printStackTrace();
//            } catch( IOException e) {
//                e.printStackTrace();
//            }
//        }else {
//            msg = "Upload failed for " + file.getName() + " as file is empty";
//        }
//
//        book.setAuthor( book.getAuthor() );
//        book.setBookName( book.getBookName() );
//        book.setGenre( book.getGenre() );
//        book.setPublishmentYear( book.getPublishmentYear() );
//      
//        
//    }
//}
//    
////    @RequestMapping("BookSearchForm")
////    public String searchBookForm(){
////        return "search-book-book";
////    }
////
////
////    @RequestMapping("processBookSearchForm")
////    public String processBookSearchForm( @RequestParam("bookName") String bookName, @RequestParam("authorName") String authorName, Model model  ){
////
////        Connection connection;
////        PreparedStatement preparedStatement;
////        ResultSet resultSet;
////
////        try {
////            connection = ConnectionProvider.getConnection();
////            preparedStatement = connection.
////                    prepareStatement("SELECT * FROM book INNER JOIN author on book.id = author.book_id WHERE book.book_name = bookName and author.name=authorName"  );
////            resultSet = preparedStatement.
////                    executeQuery( );
////
////            while( resultSet.next() ){
////                model.addAttribute("bookName", resultSet.getString("book_name"));
////                model.addAttribute("authorName", resultSet.getString("author_name"));
////                model.addAttribute( "jenre", resultSet.getString( "jenre" ) );
////                model.addAttribute( "publishmentyear", resultSet.getString( "publishment_year" ) );
////            }
////
////            if( bookName != null ){
////                return "book-found-confirmation";
////            }
////        }
////         catch( Exception e ){
////            e.printStackTrace();
////         }
////         return "book-not-found";
////    }
//
//
////    @RequestMapping(value = "/uploadFile", method = RequestMethod.POST)
//// public boolean uploadFile( @RequestParam("file") MultipartFile file, Model model) {
////        String msg= "";
////        if(!file.isEmpty()){
////            try {
////                String fileName = "C:\\Users\\Suren\\Desktop\\library\\" + file.getOriginalFilename();
////                // for storing uploaded file
////                file.transferTo(new File(fileName));
////                msg = "Upload successful for " + file.getName();
////            } catch (IllegalStateException e) {
////                e.printStackTrace();
////            } catch ( IOException e) {
////                e.printStackTrace();
////            }
////        }else {
////            msg = "Upload failed for " + file.getName() + " as file is empty";
////        }
////        model.addAttribute("message", msg);
////        model.addAttribute("file", file);
////        return updateDB();
////    }
////    
//    private boolean updateDB(){
//
//        Connection connection = ConnectionProvider.getConnection();
//        PreparedStatement prepStatement;
//        ResultSet resultSet;
//
//        try { 
//            String checkIfAuthorIsPresentInTheDB = "SELECT name FROM author WHERE name = ?";
//            prepStatement = connection.prepareStatement( checkIfAuthorIsPresentInTheDB );
//            prepStatement.setString( 1, authorName );
//            resultSet = prepStatement.executeQuery();
//
//            String insertAuthorData = "INSERT INTO author(name) VALUES(?);";
//            prepStatement = connection.prepareStatement( insertAuthorData );
//            prepStatement.setString( 1, authorName );
//            prepStatement.execute();
//
//            //have to add a lot of stuff here
//
//            String getAuthorId = "SELECT id FROM author ORDER BY author.id DESC LIMIT 1";
//
//            resultSet = prepStatement.executeQuery(getAuthorId);
//            while(resultSet.next()){
//                authorId = resultSet.getInt( "id" );
//            }
//
//            String insertBookData = "INSERT INTO book(book_name, publishment_year, jenre, author_id) VALUES (?, ?, ?, ? );";
//            prepStatement = connection.prepareStatement( insertBookData );
//            prepStatement.setString( 1, bookName );
//            prepStatement.setString( 2, publishmentYear );
//            prepStatement.setString( 3, jenre );
//            prepStatement.setInt(4, authorId);
//            prepStatement.execute();
//
//            String insertUserData = "INSERT INTO user(first_name, last_name) VALUES (?, ?);";
//            prepStatement = connection.prepareStatement( insertUserData );
//            prepStatement.setString( 1, firstName );
//            prepStatement.setString( 2, lastName );
//            prepStatement.execute();
//
//            String getUserId = "SELECT user.id FROM user ORDER BY user.id DESC LIMIT 1";
//            resultSet = prepStatement.executeQuery(getUserId);
//
//            while( resultSet.next() ){
//                userId = resultSet.getInt("id");
//            }
//
//            String getBookId = "SELECT book.id FROM book ORDER BY book.id DESC LIMIT 1";
//            resultSet = prepStatement.executeQuery(getBookId);
//
//            while( resultSet.next() ){
//                bookId = resultSet.getInt("id");
//            }
//
//            String insertDataIntouserBooks = "INSERT INTO user_books(user_id, book_id ) values(?, ?);";
//            prepStatement = connection.prepareStatement( insertDataIntouserBooks);
//            prepStatement.setInt( 1, bookId );
//            prepStatement.setInt( 2, userId );
//            prepStatement.execute();
//
//            System.out.println("Data is stored in db");
//            return true;
//       }
//        catch( Exception e ){
//            return false;
//        }
//
//}
