//package myapp.controllers;
//
//
//import myapp.models.User;
//import myapp.services.UserService;
//import org.apache.commons.lang3.StringUtils;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Controller;
//import org.springframework.ui.Model;
//import org.springframework.validation.BindingResult;
//import org.springframework.web.bind.annotation.ModelAttribute;
//import org.springframework.web.bind.annotation.RequestMapping;
//
//import javax.validation.Valid;
//
//@Controller
//@RequestMapping("/user")
//public class UserController {
//   
//    @Autowired
//    UserService userService;
//    
//    @RequestMapping( "/showUserForm" )
//    public String showUserForm( Model model ) {
//        model.addAttribute( "user", new User() );
//        return "user-form";
//    }
//    
//    
//    @RequestMapping( "/processUserForm" )
//    public String processUserForm(
//            @Valid @ModelAttribute( "user" ) User theUser, BindingResult theBindingResult ) {
//       
//        if( theBindingResult.hasErrors() ) {
//            return "user-form";
//        }
//       
//        if( StringUtils.equals(theUser.getRole(), "Teacher")){
//            
//            return "teacher-actions";
//        }
//        
//        else {
//            return "student-actions";
//        }
//        
//    }
//    
//    //validator method
//    
//}
