package myapp.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

//Controller is created for redirect to index.jsp page when we just open application whit http://localhost:8080/
@Controller
public class MainController {
    @RequestMapping("/")
    public String showIndexPage() {
        return "index";
    }
}
