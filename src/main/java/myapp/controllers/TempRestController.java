package myapp.controllers;


import myapp.models.User;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;

//Controller is added fro show how we can store object(for example User data) in session object which can be get anywere  in application (Note that every user have it own Session)
@RestController
public class TempRestController {

    @GetMapping("/login")
    public String login(HttpSession httpSession, @RequestParam(value = "role") String role) {
        User user = new User();
        user.setRole(role);
        httpSession.setAttribute("user", user);
        return "success";
    }

    @GetMapping("/logout")
    public String logout(HttpSession httpSession) {
        httpSession.removeAttribute("user");
        return "success";
    }
}
