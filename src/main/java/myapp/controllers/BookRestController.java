package myapp.controllers;

import myapp.models.Book;
import myapp.services.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.util.List;

@RestController
public class BookRestController {


    @Autowired
    private BookService bookService;

    @GetMapping("/books")
    public List<Book> getBooks(HttpSession session){
    //Example of get object from session (*Note every user have its own session)
        System.out.println(session.getAttribute("user"));
        return bookService.getBooks();
    }

    @GetMapping("/books/{bookId}")
    public Book getBook( @PathVariable int bookId ){
        return bookService.getBook(bookId);
    }

    @PostMapping("/books")
    public Book addBook( @RequestBody Book book ){
        return bookService.saveBook( book );
    }

    @PutMapping("/books")
    public Book updateBook( @RequestBody Book book){
        return null;
    }

    @DeleteMapping("/books")
    public String deleteBook(int bookId){
        return bookService.deleteBook(bookId);
    }

}
