package myapp.models;


import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.LinkedHashMap;
import java.util.Map;

@Component
public class Book {
    //create a class that holds information about the book and will serve as model attribute
    @NotNull(message = "This field is required")
    private String bookName;
    
    @NotNull(message = "This field is required")
    private String author;

    @NotNull(message = "This field is required")
    private String genre;
    
    @NotNull(message = "This field is required")
    @Pattern( regexp = "[0-9]{4}", message = "please enter a valid date")
    private String publishmentYear;
    
    public Map<String, String> getGenres() {
        return genres;
    }

    private Map<String,String> genres;

    public Book(){
        genres = new LinkedHashMap();
        genres.put("adv", "adventure");
        genres.put("fict", "fiction");
        genres.put("det", "detective");
        genres.put("drama", "drama");
        genres.put("bio", "bioraphic");
        genres.put("his", "historical");
    }

    public String getBookName() {
        return bookName;
    }

    public void setBookName( String bookName ) {
        this.bookName = bookName;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor( String author ) {
        this.author = author;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre( String genre ) {
        this.genre = genre;
    }

    public String getPublishmentYear() {
        return publishmentYear;
    }

    public void setPublishmentYear( String publishmentYear ) {
        this.publishmentYear = publishmentYear;
    }

//    public CommonsMultipartFile getFile() {
//        return file;
//    }
//
//    public void setFile( CommonsMultipartFile file ) {
//        this.file = file;
//    }
}
