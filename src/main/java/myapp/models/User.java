package myapp.models;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class User {

    @NotNull(message="is required")
    @Size(min=1, message="is required")
    private String firstName;


    @NotNull(message = "you haven't entered your last name")
    @Size(min=1, message = "you haven't entered your last name")
    private String LastName;
    
    @NotNull(message = "you haven't entered your role")
    private String role;
    
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName( String firstName ) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName( String lastName ) {
        LastName = lastName;
    }


    public String getRole() {
        return role;
    }

    public void setRole( String role ) {
        this.role = role;
    }
}
