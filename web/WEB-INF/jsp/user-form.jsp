<!-- we need to develop it to make a login and registration-->
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <title>Title</title>
    <style>
        .error {
            color: red;
        }
    </style>
</head>
<body>

<%--input field that will retrive information about the book--%>
    <form:form modelAttribute="user" action="processUserForm">
        
        First name: 
        <form:input path="firstName"/>
        <form:errors path="firstName"  cssClass="error"/>
        <br><br>
        
        Last name: 
        <form:input path="lastName"/>
        <form:errors path="lastName" cssClass="error"/>
        <br><br>
        
        User role:
        Student <form:radiobutton path="role" value="Student"/>
        Teacher <form:radiobutton path="role" value="Teacher"/>
        <br><br>
        
        <input type="submit" value="Submit"/>
    </form:form>
</body>
</html>
