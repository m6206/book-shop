<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>

<p>Welcome to our library website!</p>
<br>

<br>
<a href="/showBookForm"> <i>Go to your page</i> </a>


<button onclick="getBooks()">Get books</button>
<a href="html/login.html">go to login</a>
<div id="book-container"></div>

<script>
// ajax call example
    function getBooks() {
        var xhr = new XMLHttpRequest();

        // Making our connection
        var url = '/books';
        xhr.open("GET", url, true);

        // Sending our request
        xhr.send();

        // function execute after request is successful
        xhr.onreadystatechange = function () {
            // when readyState is 4 this meen that request already finished
            if (this.readyState == 4 && this.status == 200) {
                var books = JSON.parse(this.responseText);
                showBooks(books);
            }
        }

    }
    //example for show books in html.
    function showBooks(books) {
        var parent = document.getElementById("book-container");
        for (var i = 0; i < books.length; i++) {
            var element = createBookElement(books[i]);
            parent.appendChild(element);
        }
    }
    //method create dom element from place in html
    function createBookElement(book) {
        var bookElemenmt = document.createElement("div");
        bookElemenmt.innerText = "name: " + book.bookName + " author " + book.author;
        return bookElemenmt;

    }

</script>
</body>

</html>
