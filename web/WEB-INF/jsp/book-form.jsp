
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="ISO-8859-1">
    <title>Insert title here</title>
    <style>
        .error{
            color: #ff5042;
        }
    </style>
</head>
<body>
<h4>This is form page!</h4>
    <form:form modelAttribute="book" action="/processForm" method="POST" enctype="multipart/form-data">
        
        Input book name: <form:input path="bookName"/>
        <form:errors path="bookName" cssClass="error"/>
        <br><br>
        
        Input book author: <input path="author" />
        <form:errors path="author" cssClass="error" />
        <br><br>
        
        Input book year of publishment: <input path="publishmentYear" />
        <form:errors path="publishmentYear" cssClass="error" />
        <br><br>
        
        Select book genre:  <form:select path="genre">
        <form:options items = "${book.genres}"/>
                            </form:select>
        <br><br>
        
        <input type="file" name="file" value="select">
        <br><br>
        
        <input type="submit" value="submit">
    </form:form>
</body>
</html>


